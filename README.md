
# Description
UNS_PROJECTS contains a collection of unsio based programs

# Requirements
- you need to install CMAKE utility (Cf http://www.cmake.org/)
- unsio library must be installed (see http://projets.lam.fr/projects/unsio/wiki)

# Compilation  / installation
```
mkdir build  
cd build   
cmake ..  
make  
make install
```

# Python Wrapper
There is a python wrapper, see py/README.md in the source directory, or visit https://pypi.org/project/python-unsiotools/

# Licence
CeCILL2 (http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt)

## Webpage
PLease visit :
- [UNSIO project home page](https://projets.lam.fr/projects/unsio)
- [UNSIO Python reading manual](https://projets.lam.fr/projects/unsio/wiki/PythonReadDataNew)
- [UNSIO Python writing manual](https://projets.lam.fr/projects/unsio/wiki/PythonWriteDataNew)
- [UNSIO Pypi page](https://pypi.org/project/python-unsio/)
- [UNSIOTOOLS Pypi page](https://pypi.org/project/python-unsiotools/)
- [NEMO home page](https://teuben.github.io/nemo/)

# Copyright
**Copyright Jean-Charles LAMBERT**     
**Jean-Charles.Lambert_at_lam.fr**     

# Address  

Centre de donneeS Astrophysique de Marseille (CeSAM)   
Laboratoire d'Astrophysique de Marseille   
Pole de l'Etoile, site de Chateau-Gombert    
38, rue Frederic Joliot-Curie   
13388 Marseille cedex 13 France   
CNRS U.M.R 7326   

#!/bin/bash

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*unsio*.whl; do
    echo "repairing $whl...."
    auditwheel repair "$whl" -w  ./wheel
done

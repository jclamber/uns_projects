# install & test wheel
#for PYPATH in /opt/python/cp??-* /opt/python/cp310-cp310 ; do

if [ A$1 == "A2010" ]
then
    for PYPATH in /opt/python/cp??-* ; do
        if [ -d ${PYPATH} ] 
        then
            echo "Testing with ${PYPATH}...."
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} pip uninstall -y python-unsio python-unsiotools 
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} pip install wheel/*unsio*`basename ${PYPATH}`-manylinux*.whl
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} python py/unsiotools/examples/test_cfalcon.py
        fi
    done
else
    for PYPATH in /opt/python/cp??-* /opt/python/cp???-* ; do
        if [ -d ${PYPATH} ] 
        then
            echo "Testing with ${PYPATH}...."
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} pip uninstall -y python-unsio python-unsiotools 
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} pip install wheel/*unsio*`basename ${PYPATH}`-manylinux*.whl
            PATH=/opt/usr/bin/:${PYPATH}/bin:${PATH} python py/unsiotools/examples/test_cfalcon.py
        fi
    done
fi
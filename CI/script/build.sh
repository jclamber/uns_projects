#!/bin/bash
#
# Give as paramameter cmake (manylinux1) or cmake3 (manylinux2010)
#

mycmake=$1

# clone & build unsio
# git clone https://gitlab.lam.fr/jclamber/unsio.git
# via ssh because unsio is not public anymore !!
# git clone git@gitlab.lam.fr:infrastructure/unsio.git
git clone https://gitlab.lam.fr/infrastructure/unsio
cd unsio
# swicth to develop branch !!!
#git checkout develop

$mycmake . -DCMAKE_INSTALL_PREFIX=/usr/local
make -j 4 && make install
# Activate nemo environment
source /opt/usr/nemo/nemo_start.sh
# build uns_projects
cd ..
$mycmake . -DCMAKE_INSTALL_PREFIX=/usr/local -DUNSIOPATH=/usr/local
make -j 4
make install

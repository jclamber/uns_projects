#!/bin/bash

# build wheel
#PATH=/opt/usr/bin/:/opt/python/cp27-cp27m/bin:${PATH}  pip wheel ./unsio/ -w wheelhouse

rm -f wheel/*uns*manylinux*.whl wheel/*uns*tar.gz wheelhouse/*uns*manylinux*.whl

# Activate NEMO env
source /opt/usr/nemo/nemo_start.sh 

# build wheel
if [ A$1 == "A2010" ]
then
    for PYBIN in /opt/python/cp??-*/bin/ ;  do
        if [ -d ${PYBIN} ] 
        then
            echo "Compiling using pip version ${PYBIN}...."
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} python setup.py clean --all
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} pip install pip -U
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} pip wheel --no-deps ./ -w wheelhouse
        fi
    done
else 
    for PYBIN in /opt/python/cp??-*/bin/ /opt/python/cp???-*/bin/ ;  do
        if [ -d ${PYBIN} ] 
        then
            echo "Compiling using pip version ${PYBIN}...."
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} python setup.py clean --all
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} pip install pip -U
            PATH=/opt/usr/bin/:${PYBIN}:${PATH} pip wheel --no-deps ./ -w wheelhouse
        fi
    done
fi

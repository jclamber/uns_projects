#!/bin/bash
#
#

mycmake=cmake

# clone & build unsio
git clone https://gitlab.lam.fr/jclamber/unsio.git
cd unsio
# swicth to develop branch !!!
# git checkout develop

# remove previous build
rm -rf /tmp/local/unsio

# compile and install unsio
$mycmake . -DCMAKE_INSTALL_PREFIX=/tmp/local/unsio
make -j 4 && make install

# Activate nemo environment
source /Users/grunner/works/GIT/nemo/nemo_start.sh
# build uns_projects
cd ..

$mycmake . -DCMAKE_INSTALL_PREFIX=/tmp/local/unsio -DUNSIOPATH=/tmp/local/unsio -DCMAKE_BUILD_TYPE=Release
make -j 4
make install

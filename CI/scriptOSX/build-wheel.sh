#!/bin/bash

# remove previous wheel
rm -f wheel/*unsio*osx*.whl wheelhouse/*unsio*osx*.whl

# Activate NEMO env
source /Users/grunner/works/GIT/nemo/nemo_start.sh

# Trick to find Dehnen's lib (used by repair wheel) which have compiled with rpath
# Then, we link these libs to uns_project lib directory
ln -s ${NEMO}/usr/dehnen/falcON/lib/libfalcON.so lib
ln -s ${NEMO}/usr/dehnen/utils/lib/libWDutils.so lib

# Initialyze conda envoronement 
source ~/miniconda3/etc/profile.d/conda.sh

# Activate python environement
# Build wheel
for PYVER in 36 37 38 39 310; do
    # conda activate
    echo "Activate py${PYVER}"
    conda activate py${PYVER}

    # conda clear cache
    python setup.py clean --all

    # build wheel
    CC=clang CXX=clang++ pip wheel --no-deps ./ -w wheelhouse
    
    #
    conda deactivate
done

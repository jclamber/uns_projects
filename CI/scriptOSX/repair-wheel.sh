#!/bin/bash

# Initialyze conda envoronement 
source ~/miniconda3/etc/profile.d/conda.sh

# Activate environment conda py38 to delocate-wheel
# delocate-wheel works the same with all python versions
conda activate py38

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*unsio*.whl; do
    echo "repairing $whl...."
    DYLD_LIBRARY_PATH=/tmp/local/unsio/lib delocate-wheel "$whl" -w  ./wheel
done

# de-activate 
conda deactivate

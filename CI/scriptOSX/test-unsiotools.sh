#!/bin/bash

# Initialyze conda envoronement 
source ~/miniconda3/etc/profile.d/conda.sh

# Install unsio wheel for every python
# Run test program
for PYVER in 36 37 38 39 310; do
     # conda activate
    echo "Activate py${PYVER}"
    conda activate py${PYVER}

    # uninstall previous packages
    pip uninstall -y python-unsio
    pip uninstall -y python-unsiotools

    # install python-unsiotools
    pip install  wheel/*unsio*${PYVER}*-macosx*.whl
    python py/unsiotools/examples/test_cfalcon.py
    
    #
    conda deactivate
done